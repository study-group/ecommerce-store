# README #

This Web Application is developed to create a e-commerce store.

* Version - alpha-v.0.0.1 - Product Model and Controller-Service-DAO configuration, Spring Security for Seller to add Products.
* Version - alpha-v.0.0.2 - User Registration, Cart Management with Spring Security for User Login and Session Management.

### How do I get set up? ###

* Will Need any Java Web Application Server (used Tomcat)
* Configuration - logging setup, to be done in /src/main/resources/log4j.properties.
* Dependencies - as in pom.xml
* Database configuration - /src/main/resources/jdbc.properties
* How to run tests - ##TESTS to be written yet.
* Deployment instructions

### Contribution guidelines ###

* Writing tests - have to write test cases around it

### Who do I talk to? ###

* Repo owner - Hiren Parmar (hiren_parmar@outlook.com)