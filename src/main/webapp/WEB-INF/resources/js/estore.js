var cartApp = angular.module("cartApp", []);

cartApp.controller("cartController", function($scope, $http) {
	
	$scope.refreshCart = function(cartId) {
		$http.get('/ecommstore/v1/api/cart/' + $scope.cartId).success(function(data) {
			$scope.cart = data;
		});
	};
	
	$scope.refreshCartForCustomer = function() {
		$http.get('/ecommstore/v1/api/cart/for-customer').success(function(data) {
			$scope.cart = data;
		});
	};
	
	$scope.initCart = function(cartId) {
		$scope.cartId = cartId;
		$scope.refreshCart(cartId);
	};
	
	$scope.addProductToCart = function(productId) {
		$http.post('/ecommstore/v1/api/cart/add-product/' +productId)
		.success(function(data) {
			$scope.refreshCartForCustomer();
		})
		.error(function(data, status){
			alert(status)
		});
	};
	
	$scope.removeProductFromCart = function(productId) {
		$http.delete('/ecommstore/v1/api/cart/remove-product/' +productId)
		.success(function(data) {
			$scope.refreshCartForCustomer();
		});
	};
	
	$scope.clearCart = function() {
		$http.delete('/ecommstore/v1/api/cart/clear-cart')
		.success(function(data) {
			$scope.refreshCartForCustomer();
		});
	};
	
    $scope.calGrandTotal = function () {
        var grandTotal=0;

        for (var i=0; i<$scope.cart.cartItems.length; i++) {
            grandTotal+= ($scope.cart.cartItems[i].product.productPrice * $scope.cart.cartItems[i].quantity);
        }

        return grandTotal;
    };
});