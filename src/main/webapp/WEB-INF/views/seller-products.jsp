<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@include file="/WEB-INF/views/template/header.jsp"%>


<div class="container-wrapper">
	<div class="container">
		<div class="page-header">
			<h1>All Products</h1>

			<p class="lead">This is the product inventory page!</p>
		</div>
		<table class="table table-striped table-hover">
			<thead>
				<tr class="bg-success">
					<th>Photo Thumb</th>
					<th>Product Name</th>
					<th>Category</th>
					<th>Condition</th>
					<th>Price</th>
					<th></th>
				</tr>
			</thead>
			<c:forEach items="${products}" var="product">
				<tr>
					<td><a
						href="<spring:url value="/seller/product/${product.id}" />"><img
							src="<c:url value="/resources/images/${product.id}.png"/>"
							alt="image" style="width: 100%" /></a> <sup><a
							href="<spring:url value="/seller/product/${product.id}" />"><span
								class="glyphicon glyphicon-info-sign"></span></a> </sup></td>
					<td>${product.productName}</td>
					<td>${product.productCategory}</td>
					<td>${product.productCondition}</td>
					<td>${product.productPrice}USD</td>
					<td><a
						href="<spring:url value="/seller/product/${product.id}" />"> <span
							class="glyphicon glyphicon-info-sign"></span></a> <a
						href="<spring:url value="/seller/remove-product/${product.id}" />">
							<span class="glyphicon glyphicon-remove"></span>
					</a> <a
						href="<spring:url value="/seller/update-product/${product.id}" />">
							<span class="glyphicon glyphicon-pencil"></span>
					</a></td>
				</tr>
			</c:forEach>
		</table>
		<a href=<spring:url value="/seller/new-product/" />
			class="btn btn-primary">Add Product</a>


		<%@include file="/WEB-INF/views/template/footer.jsp"%>