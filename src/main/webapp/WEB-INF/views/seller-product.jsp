<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@include file="/WEB-INF/views/template/header.jsp"%>


<div class="container-wrapper">
	<div class="container">
		<div class="page-header">
			<h1>Product Detail</h1>

			<p class="lead">Here is the detail information of the product!</p>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<img src="<c:url value="/resources/images/${product.id}.png"/>" alt="image" style="width: 50%" />
				</div>

				<div class="col-md-5">
					<h3>${product.productName}</h3>
					<p>${product.productDescription}</p>
					<p>
						<strong>Manufacturer</strong> : ${product.productManufacturer}
					</p>
					<p>
						<strong>Category</strong> : ${product.productCategory}
					</p>
					<p>
						<strong>Condition</strong> : ${product.productCondition}
					</p>
					<h4>${product.productPrice}USD</h4>
				</div>
			</div>
			
			<div class="col-md-12"></div>
			
			<div class="col-md-12">
				<div class="col-md-4">
					<a href="<c:url value="/seller/update-product/${product.id}"/>" class="btn btn-default">Modify Product</a>
				</div>
				<div class="col-md-4">
					<a href="<c:url value="/seller/products"/>" class="btn btn-default">Cancel</a>
				</div>
				<div class="col-md-4">
					<a href="<c:url value="/seller/delete-product/${product.id}"/>" class="btn btn-default">Delete Product</a>
				</div>
			</div>
		</div>





<%@include file="/WEB-INF/views/template/footer.jsp"%>
