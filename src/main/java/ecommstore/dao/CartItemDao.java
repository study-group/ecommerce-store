package ecommstore.dao;

import ecommstore.models.CartItem;

public interface CartItemDao {

	void createOrModifyCartItem(CartItem item);

	void removeCartItem(CartItem item);
}
