package ecommstore.dao;

import java.util.List;

import ecommstore.models.Customer;

public interface CustomerDAO {

	void createCustomer(Customer customer);

	List<Customer> getCustomers();

	Customer getCustomer(Long id);
	
	void modifyCustomer(Customer customer);
	
	void removeCustomer(Customer customer);

	Customer getCustomerByUsername(String username);
}
