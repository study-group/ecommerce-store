package ecommstore.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ecommstore.dao.CustomerDAO;
import ecommstore.dao.ShoppingCartDAO;
import ecommstore.models.Customer;
import ecommstore.models.ShoppingCart;

@Repository
@Transactional
public class ShoppingCartHibernateDAOImpl implements ShoppingCartDAO {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private CustomerDAO customerDAO;

	public ShoppingCart getCart(Long id) {
		if (sessionFactory == null && id == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		ShoppingCart cart = (ShoppingCart) session.get(ShoppingCart.class, id);
		session.flush();
		return cart;
	}

	public void addOrUpdateCart(ShoppingCart cart) {
		if (sessionFactory == null && cart == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(cart);
		session.flush();
	}

	public void emptyCart(ShoppingCart cart) {
		if (sessionFactory == null && cart == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(cart);
		session.flush();
	}

	public boolean createCart(Long customerId) {
		if (sessionFactory == null && customerId == null && customerDAO == null) {
			return false;
		}
		ShoppingCart cart = customerDAO.getCustomer(customerId).getCart();
		boolean cartCreated = false;
		if (cart == null) {
			Session session = sessionFactory.getCurrentSession();
			cart = new ShoppingCart();
			session.save(cart);
			session.flush();
			Customer customer = customerDAO.getCustomer(customerId);
			customer.setCart(cart);
			customerDAO.modifyCustomer(customer);
			cartCreated = true;
		}
		return cartCreated;
	}

	public void createCart(ShoppingCart cart) {
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(cart);
		session.flush();

	}
}
