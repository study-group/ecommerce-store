package ecommstore.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ecommstore.dao.CustomerDAO;
import ecommstore.models.Customer;

@Repository
@Transactional
public class CustomerHibernateDAOImpl implements CustomerDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void createCustomer(Customer customer) {
		if (sessionFactory == null && customer == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(customer);
		session.flush();
	}

	public List<Customer> getCustomers() {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Customer");
		List<Customer> customers = query.list();
		session.flush();
		return customers;
	}

	public Customer getCustomer(Long id) {
		if (sessionFactory == null && id == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		Customer customer = (Customer) session.get(Customer.class, id);
		session.flush();
		return customer;
	}

	public void modifyCustomer(Customer customer) {
		if (sessionFactory == null && customer == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(customer);
		session.flush();
	}

	public void removeCustomer(Customer customer) {
		if (sessionFactory == null && customer == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.delete(customer);
		session.flush();
	}

	public Customer getCustomerByUsername(String username) {
		if (sessionFactory == null && username == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Customer where user_username = ?");
		query.setString(0, username);
		Customer customer = (Customer) query.uniqueResult();
		session.flush();
		return customer;
	}
}
