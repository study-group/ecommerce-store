package ecommstore.dao.impl;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ecommstore.dao.CartItemDao;
import ecommstore.models.CartItem;

@Repository
@Transactional
public class CartItemHibernateDAOImpl implements CartItemDao {

	@Autowired
	SessionFactory sessionFactory;

	public void createOrModifyCartItem(CartItem item) {
		if (sessionFactory == null && item == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(item);
		session.flush();
	}

	public void removeCartItem(CartItem item) {
		if (sessionFactory == null && item == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.delete(item);
		session.flush();
	}
}
