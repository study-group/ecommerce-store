package ecommstore.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import ecommstore.dao.ProductDAO;
import ecommstore.models.Product;

@Repository
@Transactional
public class ProductHibernateDAOImpl implements ProductDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public List<Product> getProducts() {
		if (sessionFactory == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("from Product");
		List<Product> products = query.list();
		session.flush();
		return products;
	}

	public Product getProduct(Long id) {
		if (sessionFactory == null && id == null) {
			return null;
		}
		Session session = sessionFactory.getCurrentSession();
		Product product = (Product) session.get(Product.class, id);
		session.flush();
		return product;
	}

	public void createProduct(Product product) {
		if (sessionFactory == null && product == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(product);
		session.flush();
	}

	public void modifyProduct(Product product) {
		if (sessionFactory == null && product == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.saveOrUpdate(product);
		session.flush();
	}

	public void removeProduct(Product product) {
		if (sessionFactory == null && product == null) {
			return;
		}
		Session session = sessionFactory.getCurrentSession();
		session.delete(product);
		session.flush();
	}

}
