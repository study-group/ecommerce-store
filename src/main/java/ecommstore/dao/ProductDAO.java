package ecommstore.dao;

import java.util.List;

import ecommstore.models.Product;

public interface ProductDAO {
	
	void createProduct(Product product);

	List<Product> getProducts();

	Product getProduct(Long id);
	
	void modifyProduct(Product product);
	
	void removeProduct(Product product);

}