package ecommstore.dao;

import ecommstore.models.ShoppingCart;

public interface ShoppingCartDAO {
	
	ShoppingCart getCart(Long id);

	void addOrUpdateCart(ShoppingCart cart);

	void emptyCart(ShoppingCart cart);

	boolean createCart(Long customerId);

	void createCart(ShoppingCart cart);
}
