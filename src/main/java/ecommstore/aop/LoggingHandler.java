package ecommstore.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingHandler {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(LoggingHandler.class);
	
	@Pointcut("within(ecommstore..*)")
	public void allMethodsInEStore() {
		
	}
	
//	@Pointcut("execution(* get*(..))")
//	public void allMethodsReturningInEStore() {
//		
//	}
	
	@Before(value="allMethodsInEStore()")
	public void logBeforeAllMethods(JoinPoint joinPoint) {
		LOGGER.info("Processing : {} {}", joinPoint.getTarget().getClass(), joinPoint.getSignature().getName());
	}
	
	@After(value="allMethodsInEStore()")
	public void logAfterAllMethods(JoinPoint joinPoint) {
		LOGGER.info("Processed : {} {}", joinPoint.getTarget().getClass(), joinPoint.getSignature().getName());
	}
	
	@AfterReturning(pointcut = "allMethodsInEStore()", returning="returning")
	public void logAfterReturningAllMethods(JoinPoint joinPoint, Object returning) {
		LOGGER.info("Processed : {} {} and returns {}", joinPoint.getTarget().getClass(), joinPoint.getSignature().getName(), returning);
	}
}
