package ecommstore.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ecommstore.dao.CustomerDAO;
import ecommstore.dao.ShoppingCartDAO;
import ecommstore.models.Customer;
import ecommstore.models.ShoppingCart;
import ecommstore.services.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@Autowired
	private ShoppingCartDAO cartDAO;

	public List<Customer> getCustomers() {
		return customerDAO.getCustomers();
	}

	public Customer getCustomer(Long id) {
		return customerDAO.getCustomer(id);
	}

	public void createCustomer(Customer customer) {
		if(customer.getCart() == null) {
			ShoppingCart cart = new ShoppingCart();
			customer.setCart(cart);
			cart.setCustomer(customer);
			cartDAO.createCart(cart);
		}
		customerDAO.createCustomer(customer);
	}

	public void modifyCustomer(Customer customer) {
		customerDAO.modifyCustomer(customer);
	}

	public void removeCustomer(Customer customer) {
		customerDAO.removeCustomer(customer);
	}

	public Customer getCustomerByUsername(String username) {
		return customerDAO.getCustomerByUsername(username);
	}

}
