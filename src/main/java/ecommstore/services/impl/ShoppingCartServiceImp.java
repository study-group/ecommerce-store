package ecommstore.services.impl;

import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ecommstore.dao.CartItemDao;
import ecommstore.dao.ProductDAO;
import ecommstore.dao.ShoppingCartDAO;
import ecommstore.models.CartItem;
import ecommstore.models.Product;
import ecommstore.models.ShoppingCart;
import ecommstore.services.ShoppingCartService;

@Service
public class ShoppingCartServiceImp implements ShoppingCartService {

	@Autowired
	private ShoppingCartDAO cartDAO;

	@Autowired
	private ProductDAO productDAO;
	
	@Autowired
	private CartItemDao cartItemDAO;

	public ShoppingCart getCart(Long id) {
		return cartDAO.getCart(id);
	}

	public void addProductToCart(Long productId, ShoppingCart cart, Integer quantity) {
		Product product = productDAO.getProduct(productId);
		Iterator<CartItem> cartItemIterator = cart.getCartItems().iterator();
		CartItem item = null;
		while(cartItemIterator.hasNext()) {
			CartItem tempItem = cartItemIterator.next();
			if(tempItem.getProduct().getId().equals(product.getId())) {
				item = tempItem;
				item.setQuantity((item.getQuantity()==null?0:item.getQuantity()) + quantity);
				cartItemDAO.createOrModifyCartItem(item);
				return;
			}
		}
		
		item = new CartItem();
		item.setProduct(product);
		item.setShoppingCart(cart);
		item.setQuantity((item.getQuantity()==null?0:item.getQuantity()) + quantity);
		cartItemDAO.createOrModifyCartItem(item);
	}

	public void removeProductFromCart(Long productId, ShoppingCart cart, Integer quantity) {
		Iterator<CartItem> cartItemIterator = cart.getCartItems().iterator();
		CartItem itemToBeRemoved = null;
		while (cartItemIterator.hasNext()) {
			CartItem item = cartItemIterator.next();
			if (item.getProduct().getId().equals(productId)) {
				if (quantity > 0 && item.getQuantity() > quantity) {
					item.setQuantity(item.getQuantity() - quantity);
				} else {
					itemToBeRemoved = item;
				}
			}
		}
		if (itemToBeRemoved != null) {
			cartItemDAO.removeCartItem(itemToBeRemoved);
//		} else {
//			cartItemDAO.modifyCartItem(itemToBeRemoved);
		}
	}

	public boolean createCart(Long customerId) {
		return cartDAO.createCart(customerId);
	}

	public void clearCart(Long cartId) {
		ShoppingCart cart = cartDAO.getCart(cartId);
		Iterator<CartItem> itemIterator = cart.getCartItems().iterator();
		while(itemIterator.hasNext()) {
			cartItemDAO.removeCartItem(itemIterator.next());
		}
	}

}
