package ecommstore.services.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import ecommstore.dao.ProductDAO;
import ecommstore.models.Product;
import ecommstore.services.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO;

	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#getProducts()
	 */
	public List<Product> getProducts() {
		return productDAO.getProducts();
	}

	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#getProduct(java.lang.Long)
	 */
	public Product getProduct(Long id) {
		return productDAO.getProduct(id);
	}

	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#createProduct(ecommstore.models.Product, java.lang.String)
	 */
	public void createProduct(Product product, String root) {
		createProduct(product);

		MultipartFile productImage = product.getProductImage();
		Path path = Paths.get(root + "/WEB-INF/resources/images/" + product.getId() + ".png");

		if (productImage != null && !productImage.isEmpty()) {
			try {
				productImage.transferTo(new File(path.toString()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#createProduct(ecommstore.models.Product)
	 */
	public void createProduct(Product product) {
		productDAO.createProduct(product);
	}
	
	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#modifyProduct(ecommstore.models.Product, java.lang.String)
	 */
	public void modifyProduct(Product product, String root) {
		modifyProduct(product);
		
		MultipartFile productImage = product.getProductImage();
		Path path = Paths.get(root + "/WEB-INF/resources/images/" + product.getId() + ".png");

		if (productImage != null && !productImage.isEmpty()) {
			try {
				productImage.transferTo(new File(path.toString()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#modifyProduct(ecommstore.models.Product)
	 */
	public void modifyProduct(Product product) {
		productDAO.modifyProduct(product);
	}
	
	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#removeProduct(ecommstore.models.Product, java.lang.String)
	 */
	public void removeProduct(Product product, String root) {
		Path path = Paths.get(root + "/WEB-INF/resources/images/" + product.getId() + ".png");

        if (Files.exists(path)) {
            try {
                Files.delete(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
        this.removeProduct(product);
	}
	
	/* (non-Javadoc)
	 * @see ecommstore.services.impl.ProductServiceInterface#removeProduct(ecommstore.models.Product)
	 */
	public void removeProduct(Product product) {
		productDAO.removeProduct(product);
	}
}
