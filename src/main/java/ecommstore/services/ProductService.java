package ecommstore.services;

import java.util.List;

import ecommstore.models.Product;

public interface ProductService {

	List<Product> getProducts();

	Product getProduct(Long id);

	void createProduct(Product product, String root);

	void createProduct(Product product);

	void modifyProduct(Product product, String root);

	void modifyProduct(Product product);

	void removeProduct(Product product, String root);

	void removeProduct(Product product);

}