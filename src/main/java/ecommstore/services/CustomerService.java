package ecommstore.services;

import java.util.List;

import ecommstore.models.Customer;

public interface CustomerService {

	List<Customer> getCustomers();

	Customer getCustomer(Long id);

	void createCustomer(Customer customer);

	void modifyCustomer(Customer customer);

	void removeCustomer(Customer customer);

	Customer getCustomerByUsername(String username);

}