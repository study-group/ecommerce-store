package ecommstore.services;

import ecommstore.models.ShoppingCart;

public interface ShoppingCartService {

	ShoppingCart getCart(Long id);

	void addProductToCart(Long productId, ShoppingCart cart, Integer quantity);

	void removeProductFromCart(Long productId, ShoppingCart cart ,Integer quantity);

	boolean createCart(Long customerId);

	void clearCart(Long cartId);

}
