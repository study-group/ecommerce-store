package ecommstore.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import ecommstore.models.Customer;
import ecommstore.models.ShoppingCart;
import ecommstore.services.CustomerService;

@Controller
@RequestMapping("/cart")
public class ShoppingCartController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/")
	public String retrieveCart(@AuthenticationPrincipal User user, Model model) {
		Customer customer = null;
		if (user != null) {
			customer = customerService.getCustomerByUsername(user.getUsername());
		}
		if (customer != null) {
			ShoppingCart cart = customer.getCart();
			model.addAttribute("cartId", cart.getId());
		}
		return "cart";
	}
}
