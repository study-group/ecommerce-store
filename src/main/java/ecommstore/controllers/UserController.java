package ecommstore.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import ecommstore.models.Product;
import ecommstore.services.ProductService;

@Controller
public class UserController {

	@Autowired
	private ProductService productService;

	@RequestMapping(path = "/products")
	public String getProducts(Model model) {
		if (productService != null) {
			List<Product> products = productService.getProducts();
			if (products != null) {
				model.addAttribute("products", products);
			}
		}
		return "products";
	}

	@RequestMapping(path = "/product/{id}")
	public String getProduct(@PathVariable Long id, Model model) {
		if (productService != null && id != null) {
			Product product = productService.getProduct(id);
			if (product != null) {
				model.addAttribute("product", product);
			}
		}
		return "product";
	}
}
