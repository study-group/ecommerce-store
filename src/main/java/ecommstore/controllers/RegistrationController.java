package ecommstore.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ecommstore.models.Authorities;
import ecommstore.models.Customer;
import ecommstore.models.Users;
import ecommstore.services.CustomerService;

@Controller
@RequestMapping(path = "/register")
public class RegistrationController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(path = "/new")
	public String newRegistration(Model model) {
		Customer customer = new Customer();

		Users user = new Users();
		
		Authorities authorities = new Authorities();
		authorities.setAuthority("ROLE_USER");
		
		user.setAuthorities(authorities);
		customer.setUser(user);

		model.addAttribute("customer", customer);
		return "new-registration";
	}

	@RequestMapping(path = "/", method = RequestMethod.POST)
	public String register(@ModelAttribute("customer") Customer customer) {
		if(customer != null) {
			customer.getUser().setEnabled(true);
			String username = customer.getUser().getUsername();
			customer.getUser().getAuthorities().setUsername(username);
			customer.getUser().setCustomer(customer);
			customerService.createCustomer(customer);
		}
		return "registered";
	}
}
