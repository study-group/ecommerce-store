package ecommstore.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ecommstore.models.Customer;
import ecommstore.models.ShoppingCart;
import ecommstore.services.CustomerService;
import ecommstore.services.ShoppingCartService;

@Controller
@RequestMapping("/v1/api/cart")
public class ShoppingCartWSController {

	@Autowired
	private ShoppingCartService cartService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping("/{cartId}")
	public @ResponseBody ShoppingCart retrieveCart(@PathVariable(value = "cartId") Long cartId) {
		if (cartService == null) {
			// TODO: redirect to error page.
		}
		return cartService.getCart(cartId);
	}

	@RequestMapping("/for-customer")
	public @ResponseBody ShoppingCart retrieveCartForCustomer(@AuthenticationPrincipal User activeUser) {
		Customer customer = null;
		ShoppingCart cart = null;
		if (customerService != null && activeUser != null) {
			customer = customerService.getCustomerByUsername(activeUser.getUsername());
		}
		if (customer != null) {
			cart = customer.getCart();
		}
		return cart;
	}

	@RequestMapping(value = "/add-product/{productId}", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void addProductToCart(@PathVariable(value = "productId") Long productId,
			@AuthenticationPrincipal User activeUser) {
		// Authentication auth =
		// SecurityContextHolder.getContext().getAuthentication();
		Customer customer = null;
		ShoppingCart cart = null;
		if (customerService != null && activeUser != null) {
			customer = customerService.getCustomerByUsername(activeUser.getUsername());
		}
		if (customer != null) {
			cart = customer.getCart();
		}
		if (cartService != null && cart != null && productId != null) {
			cartService.addProductToCart(productId, cart, 1);
		}
	}

	@RequestMapping(value = "/remove-product/{productId}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void removeProductToCart(@PathVariable(value = "productId") Long productId,
			@AuthenticationPrincipal User activeUser) {
		Customer customer = null;
		ShoppingCart cart = null;
		if (customerService != null && activeUser != null) {
			customer = customerService.getCustomerByUsername(activeUser.getUsername());
		}
		if (customer != null) {
			cart = customer.getCart();
		}
		if (cartService != null && cart != null && productId != null) {
			cartService.removeProductFromCart(productId, cart, -1);
		}
	}

	@RequestMapping(value = "/clear-cart", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void clearCart(@AuthenticationPrincipal User activeUser) {
		Customer customer = null;
		ShoppingCart cart = null;
		if (customerService != null && activeUser != null) {
			customer = customerService.getCustomerByUsername(activeUser.getUsername());
		}
		if (customer != null) {
			cart = customer.getCart();
		}
		if (cartService != null && cart != null) {
			cartService.clearCart(cart.getId());
		}
	}
}
