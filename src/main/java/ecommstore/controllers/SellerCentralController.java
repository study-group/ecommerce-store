package ecommstore.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import ecommstore.models.Product;
import ecommstore.services.ProductService;

@Controller
@RequestMapping(path = "/seller")
public class SellerCentralController {

	@Autowired
	private ProductService productService;

	@RequestMapping(path = "/products")
	public String getSellerProducts(Model model) {
		if (productService != null) {
			List<Product> products = productService.getProducts();
			if (products != null) {
				model.addAttribute("products", products);
			}
		}
		return "seller-products";
	}

	@RequestMapping(path = "/product/{id}")
	public String getProduct(@PathVariable Long id, Model model) {
		if (productService != null && id != null) {
			Product product = productService.getProduct(id);
			if (product != null) {
				model.addAttribute("product", product);
			}
		}
		return "seller-product";
	}

	@RequestMapping(path = "/new-product")
	public String newSellerProduct(Model model) {
		Product product = new Product();
		product.setProductCategory("instrument");
		product.setProductCondition("new");
		product.setProductStatus("active");

		model.addAttribute("product", product);
		return "new-product";
	}

	@RequestMapping(path = "/add-product", method = RequestMethod.POST)
	public String createSellerProduct(@Valid @ModelAttribute("product") Product product, BindingResult result,
			HttpServletRequest request) {
		if (result.hasErrors()) {
			return "new-product";
		}
		String rootDirectory = request.getSession().getServletContext().getRealPath("/");
		productService.createProduct(product, rootDirectory);
		return "redirect:/seller/products";
	}

	@RequestMapping(path = "/update-product/{id}")
	public String updateSellerProduct(@PathVariable Long id, Model model) {
		Product product = productService.getProduct(id);
		model.addAttribute("product", product);
		return "update-product";
	}

	@RequestMapping(path = "/modify-product", method = RequestMethod.POST)
	public String modifySellerProduct(@Valid @ModelAttribute("product") Product product, BindingResult result,
			HttpServletRequest request) {
		if (result != null && result.hasErrors()) {
			return "update-product";
		}
		String rootDirectory = request.getSession().getServletContext().getRealPath("/");

		if (productService != null) {
			productService.modifyProduct(product, rootDirectory);
		}
		return "redirect:/seller/products";
	}

	@RequestMapping(path = "/remove-product/{id}")
	public String removeSellerProduct(@PathVariable Long id, HttpServletRequest request) {
		String rootDirectory = request.getSession().getServletContext().getRealPath("/");
		if (productService != null && id != null) {
			Product product = productService.getProduct(id);
			if (product != null) {
				productService.removeProduct(product, rootDirectory);
			}
		}
		return "redirect:/seller/products";
	}

}
