package ecommstore.util;

import java.io.Serializable;

import org.hibernate.mapping.PersistentClass;
import org.hibernate.tuple.Instantiator;
import org.hibernate.tuple.entity.EntityMetamodel;
import org.hibernate.tuple.entity.PojoEntityTuplizer;

public class AddressTypeTuplizer extends PojoEntityTuplizer {

	public AddressTypeTuplizer(EntityMetamodel entityMetamodel, PersistentClass persistentClass) {
		super(entityMetamodel, persistentClass);
	}
	
	@Override
	protected Instantiator buildInstantiator(final PersistentClass persistentClass) {
		return new Instantiator() {
			
			public boolean isInstance(Object arg0) {
				throw new UnsupportedOperationException("Not Supported");
			}
			
			public Object instantiate(Serializable id) {
				try {
					return Enum.valueOf((Class) persistentClass.getClass().getClassLoader().loadClass(persistentClass.getClassName()),
					        (String) id);
				} catch (ClassNotFoundException e) {
					throw new AssertionError(e);
				}
			}
			
			public Object instantiate() {
				throw new UnsupportedOperationException("Not Supported");
			}
		};
		
		//return super.buildInstantiator(persistentClass);
	}

}
