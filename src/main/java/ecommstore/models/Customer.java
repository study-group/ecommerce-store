package ecommstore.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@NotEmpty(message = "The customer name must not be empty.")
	private String customerName;

	@NotEmpty(message = "The customer email must not be empty.")
	private String customerEmail;
	private String customerPhone;

	@OneToOne(cascade = CascadeType.ALL)
	private Users user;

	@OneToMany
	private Set<Address> shippingAddresses;

	@OneToMany
	private Set<Address> billingAddresses;
	
	@OneToOne
    @JsonIgnore
	private ShoppingCart cart;
	
	//TODO: add saved for later cart functionality

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public Users getUser() {
		return user;
	}

	public void setUser(Users user) {
		this.user = user;
	}

	public Set<Address> getShippingAddresses() {
		return shippingAddresses;
	}

	public void setShippingAddresses(Set<Address> shippingAddresses) {
		this.shippingAddresses = shippingAddresses;
	}

	public Set<Address> getBillingAddresses() {
		return billingAddresses;
	}

	public void setBillingAddresses(Set<Address> billingAddresses) {
		this.billingAddresses = billingAddresses;
	}

	public List<Address> getShippingAddressesAsList() {
		return shippingAddresses == null ? null : new ArrayList<Address>(shippingAddresses);
	}

	public void setShippingAddressesAsList(Collection<Address> shippingAddresses) {
		Set<Address> shippingAddressesAsSet = new HashSet<Address>(shippingAddresses);
		this.shippingAddresses = shippingAddressesAsSet;
	}

	public List<Address> getBillingAddressesAsList() {
		return billingAddresses == null ? null : new ArrayList<Address>(billingAddresses);
	}

	public void setBillingAddressesAsList(Collection<Address> billingAddresses) {
		Set<Address> billingAddressesAsSet = new HashSet<Address>(billingAddresses);
		this.billingAddresses = billingAddressesAsSet;
	}

	public ShoppingCart getCart() {
		return cart;
	}

	public void setCart(ShoppingCart cart) {
		this.cart = cart;
	}
}
