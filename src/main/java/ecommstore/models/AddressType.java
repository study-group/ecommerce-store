package ecommstore.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Tuplizer;

import ecommstore.util.AddressTypeTuplizer;

@Entity
@Table(name="ADDRESS_TYPE")
@Tuplizer(impl = AddressTypeTuplizer.class)
public enum AddressType {
	
	BILLING, SHIPPING;
	
	@Id
	public String name = toString();
}
